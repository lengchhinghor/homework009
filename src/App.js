import './App.css';
import React, { Component,useContext, createContext, useState } from 'react'
import {Navbar, Nav, NavDropdown, FormControl, Form, Button,Container} from 'react-bootstrap'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import Language from './component/Language';
import NavBar from './component/NavBar';
import Home from './component/Home';
import Video from './component/Video';
import Account from './component/Account';
import HomeURLParam from './component/HomeURLParam';
import Auth from './component/Auth';
import Welcome from './component/Welcome';

export default function App({setCards}) {
 return(
        <Router>
        {/* <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <Link to="/dashboard">Dashboard</Link>
          </li>
          <li>
            <Link to="/language">Language</Link>
          </li>
        </ul>
        <hr /> */}
        <Container>
        <NavBar/>
      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>
        {/* <Route path="/about/:id" render={(props) => <About {...props} />}>
        </Route> */}
        <Route path="/video" component={Video} />
        <Route path="/language" component={Language} />
        <Route path="/account/:id" children={<Account/>} />
        <Route path="/home/:params" render={()=><HomeURLParam/>}/>
        <Route path="/auth" component={Auth}/>
        <Route path="/welcome" component={Welcome}/>
      </Switch>
      </Container>
      </Router>
 )


}

