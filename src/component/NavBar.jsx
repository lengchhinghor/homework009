import React from 'react'
import {Navbar, Nav, NavDropdown, FormControl, Form, Button,Container} from 'react-bootstrap'

export default function NavBar() {
    return (
        <Navbar bg="light" expand="lg">
              <Navbar.Brand href="#">React-Router</Navbar.Brand>
              <Navbar.Toggle aria-controls="navbarScroll" />
              <Navbar.Collapse id="navbarScroll">
                <Nav
                  className="mr-auto my-2 my-lg-0"
                  style={{ maxHeight: '100px' }}
                  navbarScroll
                >
            
                  <Nav.Link href="/">Home</Nav.Link>
                  <Nav.Link href="/video">Video</Nav.Link>
                  <Nav.Link href="/account/:id">Account</Nav.Link>
                  <Nav.Link href="/welcome">Welcome</Nav.Link>
                  <Nav.Link href="/auth">Auth</Nav.Link>
                </Nav>
                <Form className="d-flex">
                  <FormControl
                    type="search"
                    placeholder="Search"
                    className="mr-2"
                    aria-label="Search"
                  />
                  <Button variant="outline-success">Search</Button>
                </Form>
              </Navbar.Collapse>
            </Navbar>
    )
}
