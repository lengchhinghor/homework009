import React from 'react'
import {Button, ButtonGroup} from 'react-bootstrap'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch
  } from "react-router-dom";
export default function Video(props) {
    const {path, url} = useRouteMatch();
    const { topicId } = useParams();

    return (
        <div>
        <h2>Video</h2>
        <ButtonGroup aria-label="Basic example">
        <Button variant="secondary" href="/topic">Movie</Button>
        
        <Button variant="secondary">Animation</Button>
        </ButtonGroup>
        <div>
      <h2>Topics</h2>
      <ul>
        <li>
          <Link to={`${url}/rendering`}>Rendering with React</Link>
        </li>
        <li>
          <Link to={`${url}/components`}>Components</Link>
        </li>
        <li>
          <Link to={`${url}/props-v-state`}>Props v. State</Link>
        </li>
      </ul>

      <Switch>
        <Route exact path={path}>
          <h3>Please select a topic.</h3>
        </Route>
        <Route path={`${path}/:topicId`}>
        <h3>{topicId}</h3>
        </Route>
      </Switch>
    </div>
</div>
        
    )
}
