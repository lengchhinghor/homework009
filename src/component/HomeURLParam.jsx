import React from 'react'
import {Container} from 'react-bootstrap'
import {useParams} from 'react-router'
export default function HomeURLParam() {
    let {params} = useParams()
    return (
        <div>
            <h1>Detail:{params} </h1>
        </div>
    )
}
