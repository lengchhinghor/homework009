import React from 'react'
import {Form, Button, Container, Row, Col} from 'react-bootstrap'

export default function Welcome() {
    return (
        <div>
            <h1>Welcome</h1>
            <Button variant="primary" type="submit">
                    Logout
            </Button>
        </div>
    )
}
