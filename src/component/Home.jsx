import {React,useState} from 'react'
import {Row, Col, Card, Button} from 'react-bootstrap'


export default function Home() {
    
    const [cards, setCard] = useState([
        {   id: 1,
            title: "my card",
            content: "my content",
            image: "/image/burger.jpeg",
        },
        {
            id: 2,
            title: "John Doe",
            content: "24",
            image: "/image/chicken.jpg",
        },
        {
            id: 3,
            title: "John Doe",
            content: "24",
            image: "/image/coca.jpg",
        },
        {
            id: 4,
            title: "John Doe",
            content: "24",
            image: "/image/pizza.jpg",
        },
        {
            id: 5,
            title: "John Doe",
            content: "24",
            image: "/image/burger.jpeg",

        },
        {
            id: 6,
            title: "John Doe",
            content: "24",
            image: "/image/chicken.jpg",
        },
        {
            id: 7,
            title: "John Doe",
            content: "24",
            image: "/image/coca.jpg",
        },
        {
            id: 8,
            title: "John Doe",
            content: "24",
            image: "/image/pizza.jpg",

        },
    ])
   
    return (
        <Row>
            {
                cards.map(cObj=>(
                    <Col md={3} >
            <Card style={{ width: '17rem'}} className="pd-5 mt-5 h">
                <Card.Img variant="top" src={cObj.image}/>
                <Card.Body>
                    <Card.Title>{cObj.title}</Card.Title>
                    <Card.Text>
                    {cObj.content}
                    </Card.Text>
                    <Button href="/home/:params" variant="primary">Read</Button>
                </Card.Body>
                </Card>
            </Col>
                ))
            }

        </Row>
    )
}

  