  import React from 'react'
  import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams
  } from "react-router-dom";
  export default function Account() {
    let { id } = useParams();

      return (
        <div>
        <h2>Accounts</h2>

        <ul>
          <li>
            <Link to="/account/netflix">Netflix</Link>
          </li>
          <li>
            <Link to="/account/zillow-group">Zillow Group</Link>
          </li>
          <li>
            <Link to="/account/yahoo">Yahoo</Link>
          </li>
          <li>
            <Link to="/account/modus-create">Modus Create</Link>
          </li>
        </ul>
        <div>
        <h3>ID: {id}</h3>
      </div>
      </div>
      )
  }
  
